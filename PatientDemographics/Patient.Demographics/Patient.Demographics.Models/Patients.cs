﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Patient.Demographics.Models
{
    /// <summary>
    /// Patient Class Contains Details Related to patients & there mandatory field details which will get populated from XML
    /// </summary>
    [XmlRoot("Patients")]
    public class Patients
    {

        [XmlElement]
        [Required]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Forename should be minimum of 3 and maximum of 50 letters")]
        public string Forenames { get; set; }

        [XmlElement]
        [Required]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "Surname should be minimum of 2 and maximum of 50 letters")]
        public string Surname { get; set; }

        [XmlElement]
        [Required(ErrorMessage = "Invalid Gender")]
        public string Gender { get; set; }

        [XmlElement]
        public string DOB { get; set; }

        [XmlElement]
        [Display(Name = "Home Phone Number")]
        [DataType(DataType.PhoneNumber, ErrorMessage = "Invalid Phone Number")]
        public string HomeNumber { get; set; }

        [XmlElement]
        [Display(Name = "Work Phone Number")]
        [DataType(DataType.PhoneNumber, ErrorMessage = "Invalid Phone Number")]
        public string WorkNumber { get; set; }

        [XmlElement]
        [Display(Name = "Mobile Number")]
        [DataType(DataType.PhoneNumber, ErrorMessage = "Invalid Phone Number")]
        public string MobileNumber { get; set; }
    }
}
