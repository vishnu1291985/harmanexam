﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patient.Demographics.Models
{
    /// <summary>
    /// DTO for handling XML from Database
    /// </summary>
    public class PatientDTO
    {
        public int PatientId { get; set; }
        public string PatientXML { get; set; }
    }
}
