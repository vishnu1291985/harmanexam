﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Patient.Demographics.Models;
using Patient.Demographics.DataAccess;
using Patient.Demographics.BLL.PatientRepository;

namespace Patient.Demographics.API.Controllers
{
    /// <summary>
    /// API exposed for doing CRUD operations for Patient Demographics
    /// </summary>
    [RoutePrefix("api")]
    public class PatientController : ApiController
    {
        private IPatientRepository _patientReposotory;

        /// <summary>
        /// Constructor used to set current context
        /// </summary>
        public PatientController()
        {
            _patientReposotory = new PatientRepository(new PatientDemoTestEntities());
        }

        /// <summary>
        /// Constructor used to set current context
        /// </summary>
        public PatientController(IPatientRepository patientRepository)
        {
            _patientReposotory = patientRepository;
        }

        /// <summary>
        /// This will return all Patients availabe in the system
        /// </summary>
        /// <returns>
        /// List of PAtients converted from XML from database
        /// </returns>
        [Route("get/patient")]
        [HttpGet]
        public IHttpActionResult Get()
        {
            var lstPatients = _patientReposotory.GetAll();
            if (lstPatients.Count > 0)
            {
                return Ok(lstPatients);
            }
            else
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Saves Patient Model as XML in Database
        /// </summary>
        /// <param name="patient"></param>
        /// <returns>whether Successful or not</returns>
        [Route("save/patient")]
        [HttpPost]
        public IHttpActionResult Post(Patients patient)
        {
            bool save = true;
            if (!string.IsNullOrWhiteSpace(patient.Forenames) && !string.IsNullOrWhiteSpace(patient.Surname)
                && !string.IsNullOrWhiteSpace(patient.Gender))
            {
                _patientReposotory.Insert(patient);
                return Ok(save);
            }
            else
            {
                return InternalServerError();
            }
        }
    }
}