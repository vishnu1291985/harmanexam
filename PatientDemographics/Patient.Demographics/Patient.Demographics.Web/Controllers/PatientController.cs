﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Xml.Serialization;
using Newtonsoft.Json;
using Patient.Demographics.Models;

namespace Patient.Demographics.Web.Controllers
{
    public class PatientController : Controller
    {
        /// <summary>
        /// This will populate available Patient List
        /// </summary>
        /// <returns>View populate with Model</returns>
        public async Task<ActionResult> Index()
        {
            List<Patients> lstPatients = new List<Patients>();
            using (HttpClient httpClient = new HttpClient())
            {
                string url = Convert.ToString(ConfigurationManager.AppSettings["PatientsAPI"] + "get/patient");
                string xmlstringRemoval = Convert.ToString(ConfigurationManager.AppSettings["APIXMLRemoval"]);
                var datas = Convert.ToString(await httpClient.GetStringAsync(url));
                if (datas.Split('>').Where(x => x.Contains("ArrayOfPatients")).Count() > 1)
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(List<Patients>));
                    foreach (var replace in xmlstringRemoval.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        datas = datas.Replace(replace, "");
                    }
                    StringReader stringReader = new StringReader(datas);
                    lstPatients = (List<Patients>)serializer.Deserialize(stringReader);
                }
            }
            return View(lstPatients);
        }

        /// <summary>
        /// This is will initialise Page required to Add New Patient Demographics
        /// </summary>
        /// <returns>View populate with Model</returns>
        public ActionResult Create()
        {
            Patients patient = new Patients();
            ViewBag.ErrorMessage = string.Empty;
            PopulateDropdown();
            return View(patient);
        }

        /// <summary>
        /// Submit for Saving new Patient Demographics
        /// </summary>
        /// <param name="patient">new Patient Demographics</param>
        /// <returns>if Saves, returns to list. Otherwise to current view loading with error</returns>
        [HttpPost]
        public async Task<ActionResult> Create(Patients patient)
        {
            try
            {
                ViewBag.ErrorMessage = string.Empty;
                PopulateDropdown();
                if (ModelState.IsValid)
                {
                    using (HttpClient httpClient = new HttpClient())
                    {
                        string url = Convert.ToString(ConfigurationManager.AppSettings["PatientsAPI"] + "save/patient");

                        var content = JsonConvert.SerializeObject(patient);
                        var buffer = System.Text.Encoding.UTF8.GetBytes(content);
                        var byteContent = new ByteArrayContent(buffer);
                        byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        var response = await httpClient.PostAsync(url, byteContent);
                        if (response.IsSuccessStatusCode)
                        {
                            return RedirectToAction("Index");
                        }
                        else
                        {
                            ViewBag.ErrorMessage = response.StatusCode;
                            return View(patient);
                        }
                    }
                }
                else
                {
                    return View(patient);
                }
            }
            catch
            {
                return View();
            }
        }

        /// <summary>
        /// To populate Dropdowns using Viewbag
        /// </summary>
        private void PopulateDropdown()
        {

            List<SelectListItem> lstGender = new List<SelectListItem>();
            lstGender.Add(new SelectListItem { Text = "Male", Value = "Male" });
            lstGender.Add(new SelectListItem { Text = "Female", Value = "Female" });
            lstGender.Add(new SelectListItem { Text = "Others", Value = "Others" });
            ViewBag.Gender = lstGender;
        }

    }
}
