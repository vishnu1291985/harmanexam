﻿using AutoMapper;
using Patient.Demographics.DataAccess;
using Patient.Demographics.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace Patient.Demographics.BLL.PatientRepository
{
    /// <summary>
    /// This Repository holds all Business logic related to Patient Demographics
    /// </summary>
    public class PatientRepository : IPatientRepository
    {
        protected PatientDemoTestEntities _context;
        private bool disposed = false;

        /// <summary>
        /// This static contructor to initialise Automapper
        /// </summary>
        static PatientRepository()
        {

            Mapper.Initialize(x => x.CreateMap<tblPatient, PatientDTO>().ReverseMap());
        }

        /// <summary>
        /// This constructor for setting DBContext
        /// </summary>
        /// <param name="dataContext">current DBContext</param>
        public PatientRepository(PatientDemoTestEntities dataContext)
        {
            _context = dataContext;
        }

        /// <summary>
        /// For disposing all unused objects
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        /// <summary>
        /// For disposing all unused objects
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// This will retrieve all Patients data from DB and converts XML to Patient Model
        /// </summary>
        /// <returns> IQueryable<Patients></returns>
        List<Patients> IPatientRepository.GetAll()
        {
            var lstPatients = _context.tblPatients.Select(x => x.PatientXML).ToList();
            if (lstPatients.Count() > 0)
            {
                return lstPatients.Select(x => LoadFromXMLString(x)).ToList();
            }
            return new List<Patients>();
        }

        /// <summary>
        /// For Inserting Patient Model into DB as XML
        /// </summary>
        /// <param name="patient">Model From View</param>
        /// <returns>Success or Not</returns>
        int IPatientRepository.Insert(Patients patient)
        {
            int result = -1;
            XmlSerializer serializer = new XmlSerializer(typeof(Patients));
            var stringwriter = new System.IO.StringWriter();
            serializer.Serialize(stringwriter, patient);
            var xml = Convert.ToString(stringwriter);
            PatientDTO patientDTO = new PatientDTO();
            if (!string.IsNullOrWhiteSpace(xml))
            {
                patientDTO.PatientXML = xml;
                _context.tblPatients.Add(Mapper.Map<tblPatient>(patientDTO));
                result = _context.SaveChanges();
            }
            return result;
        }

        /// <summary>
        /// Method to convert xml to Patient Model
        /// <param name="xmlText"> XML Text from Database</param>
        /// <returns>Patients Model</returns>
        private Patients LoadFromXMLString(string xmlText)
        {
            var stringReader = new System.IO.StringReader(xmlText);
            var serializer = new XmlSerializer(typeof(Patients));
            var patient = serializer.Deserialize(stringReader) as Patients;
            return patient;
        }
    }
}
