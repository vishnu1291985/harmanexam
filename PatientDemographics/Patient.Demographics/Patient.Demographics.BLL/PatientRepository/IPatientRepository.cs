﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Patient.Demographics.Models;

namespace Patient.Demographics.BLL.PatientRepository
{
    /// <summary>
    /// Patient- Interface for Actions Peformed 
    /// </summary>
    public interface IPatientRepository : IDisposable
    {
        int Insert(Patients patient);
        List<Patients> GetAll();
    }
}
