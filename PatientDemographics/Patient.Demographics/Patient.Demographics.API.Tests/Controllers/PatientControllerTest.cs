﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.Results;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Patient.Demographics.API;
using Patient.Demographics.API.Controllers;
using Patient.Demographics.Models;

namespace Patient.Demographics.API.Tests.Controllers
{
    [TestClass]
    public class PatientControllerTest
    {


        [TestMethod]
        public void GetOKResultIfRecordsFound()
        {
            PatientController controller = new PatientController();
            IHttpActionResult actionResult = controller.Get();
            Assert.IsInstanceOfType(actionResult, typeof(OkNegotiatedContentResult<List<Patients>>));
        }

        /// <summary>
        /// This will be passed for very first time if not records are there
        /// </summary>
        [TestMethod]
        public void GetNoRecordsIfNotFound()
        {
            PatientController controller = new PatientController();
            IHttpActionResult actionResult = controller.Get();
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }



        [TestMethod]
        public void GetInternalServerIfMandatoryFieldNotFoundForInsert()
        {
            PatientController controller = new PatientController();
            IHttpActionResult actionResult = controller.Post(new Patients()
            {
                DOB = DateTime.Now.AddDays(-32).ToString("MM/dd/yy")
            ,
                Surname = "TestSur"
            ,
                Gender = "Male"
            });
            Assert.IsInstanceOfType(actionResult, typeof(InternalServerErrorResult));
        }


        [TestMethod]
        public void GetOKIfInsertedForInsert()
        {
            PatientController controller = new PatientController();
            IHttpActionResult actionResult = controller.Post(new Patients()
            {
                DOB = DateTime.Now.AddDays(-32).ToString("MM/dd/yy")
            ,
                Forenames = "Test"
            ,
                Surname = "TestSur"
            ,
                Gender = "Male"
            });
            Assert.IsInstanceOfType(actionResult, typeof(OkNegotiatedContentResult<bool>));
        }

    }
}
